from cma import fmin
import numpy as np
import random
from src.agents import BaseAgent, Objective
from src.potential import EnvPotential, Potential
from src.simulation import Simulation
from time import time
import copy


class Optimizer():

    def __init__(self, n_step, boundaries, agents_A, agents_B, suppliers, warehouses, range_env=range(-5, -4), coeffs_env=[10]):
        self.boundaries = boundaries
        self.n_step = n_step
        self.agents_A = agents_A
        self.agents_B = agents_B
        self.strategy_A = 'potential'
        self.strategy_B = 'potential'
        self.suppliers = suppliers
        self.warehouses = warehouses
        self.range_env = range_env
        self.coeffs_env = coeffs_env

    def _get_potential_dict(self, potential_coeffs_A, potential_coeffs_B): 
        potentials = {
            'A' : {
                'environment' : EnvPotential(power_range=self.range_env, coeffs=self.coeffs_env, size=5),
                'A' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_A[0: 2]),
                'B' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_A[2: 4])
            },
            'B' : {
                'environment' : EnvPotential(power_range=self.range_env, coeffs=self.coeffs_env, size=5),
                'A' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_B[0: 2]),
                'B' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_B[2: 4]),
                'suppliers' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_B[4: 6]),
                'warehouses' : Potential(power_range=range(-2, 0), coeffs=potential_coeffs_B[6: 8])
            }
        }
        return potentials

    def get_cost_function(self, potential_coeffs_A, potential_coeffs_B, mode):
        assert mode in ['A', 'B']

        def cost_function(x): 
            if mode == 'A': 
                potentials = self._get_potential_dict(x, potential_coeffs_B)
            elif mode == 'B': 
                potentials = self._get_potential_dict(potential_coeffs_A, x)

            simulation = Simulation(self.boundaries, 
                                    agents_A = copy.deepcopy(self.agents_A), 
                                    agents_B = copy.deepcopy(self.agents_B), 
                                    strategy_A = self.strategy_A, 
                                    strategy_B = self.strategy_B,
                                    suppliers = self.suppliers, 
                                    warehouses = self.warehouses,
                                    potentials = potentials)

            score = simulation.run(n_step=self.n_step)
            return score if mode == 'A' else -score

        return cost_function

    def save_for_animation(self, potential_coeffs_A, potential_coeffs_B, name):
        potentials = self._get_potential_dict(potential_coeffs_A, potential_coeffs_B)


        simulation = Simulation(self.boundaries, 
                                agents_A = copy.deepcopy(self.agents_A), 
                                agents_B = copy.deepcopy(self.agents_B), 
                                strategy_A = self.strategy_A, 
                                strategy_B = self.strategy_B,
                                suppliers = self.suppliers, 
                                warehouses = self.warehouses,
                                potentials = potentials)

        simulation.run(n_step=self.n_step)
        simulation.save(name = name)

    def optimize(self, iter_nb, sigma_0, save = True, name = ''):
        # inital values for potentials
        potential_coeffs_A = np.ones((4)).astype('float64')
        potential_coeffs_B = np.ones((8)).astype('float64')

        for i in range(iter_nb): 
            f = self.get_cost_function(potential_coeffs_A, potential_coeffs_B, mode = 'A')
            potential_coeffs_A = fmin(f, potential_coeffs_A, sigma_0)[0]
            self.save_for_animation(potential_coeffs_A, potential_coeffs_B, name + f'_{i}_A')
            print(f'\niter [{i}/{iter_nb-1}] finished for team A\n')

            f = f = self.get_cost_function(potential_coeffs_A, potential_coeffs_B, mode = 'B')
            potential_coeffs_B = fmin(f, potential_coeffs_B, sigma_0)[0]
            self.save_for_animation(potential_coeffs_A, potential_coeffs_B, name + f'_{i}_B')
            print(f'\niter [{i}/{iter_nb-1}] finished for team B\n')

            
            sigma_0 *= 0.8


if __name__ == '__main__':
    n_step = 200
    effectif_A = 1
    effectif_B = 3
    boundaries = np.array([[-5, 5], [-5, 5]]).astype('float64')
    initial_pos_A = [np.array([-3 + random.random(), -3 + random.random()]).astype('float64') for i in range(effectif_A)]
    initial_pos_B = [np.array([random.random(), random.random()]).astype('float64') for i in range(effectif_B)]
    agents_A = [BaseAgent('A', initial_pos_A[i], boundaries) for i in range(effectif_A)]
    agents_B = [BaseAgent('B', initial_pos_B[i], boundaries) for i in range(effectif_B)]

    suppliers = [Objective(np.array([0, 0]).astype('float64'))]
    warehouses = [Objective(np.array([-3, 0]).astype('float64')), Objective(np.array([3, 0]).astype('float64'))]

    potential_values_A = np.ones((4)).astype('float64')
    potential_values_B = np.ones((8)).astype('float64')

    sigma_0 = 2.5 # a fourth of the domain size


    opt = Optimizer(n_step, 
                    boundaries, 
                    agents_A, 
                    agents_B, 
                    suppliers, 
                    warehouses)

    opt.optimize(iter_nb = 10, sigma_0 = 2.5, save = True, name = 'CMA-ES_opt')
