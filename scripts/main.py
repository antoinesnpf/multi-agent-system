import random
from typing import Optional
import numpy as np
from src.agents import BaseAgent, Objective
from src.potential import EnvPotential, Potential
from src.simulation import Simulation
import argparse

# testing a simulation with the random strategy

def main(args):
    boundaries = np.array([[-5, 5], [-5, 5]]).astype('float64')

    cop_nb = 1
    thief_nb = 3

    initial_pos_A = [np.array([-3 + random.random(), -3 + random.random()]).astype('float64') for i in range(cop_nb)]
    initial_pos_B = [np.array([random.random(), random.random()]).astype('float64') for i in range(thief_nb)]

    print('A', initial_pos_A[0], boundaries)

    agents_A = [BaseAgent('A', initial_pos_A[i], boundaries) for i in range(cop_nb)]
    agents_B = [BaseAgent('B', initial_pos_B[i], boundaries) for i in range(thief_nb)]

    suppliers = [Objective(np.array([0, 0]).astype('float64'))]
    warehouses = [Objective(np.array([-3, 0]).astype('float64')), Objective(np.array([3, 0]).astype('float64'))]


    # potentials
    potentials = {
        'A' : {
            'environment' : EnvPotential(power_range=range(-3, -1), coeffs=[1, 1], size=5),
            'A' : Potential(power_range=range(-2, 0), coeffs=[0, 0]),
            'B' : Potential(power_range=range(-2, 0), coeffs=[1, 0])
        },
        'B' : {
            'environment' : EnvPotential(power_range=range(-3, -1), coeffs=[1, 0.5], size=5),
            'A' : Potential(power_range=range(-4, 0), coeffs=[-3, -1, -1, 0]),
            'B' : Potential(power_range=range(-2, 0), coeffs=[0, 0]),
            'suppliers' : Potential(power_range=range(-2, 0), coeffs=[2, 0]),
            'warehouses' : Potential(power_range=range(-2, 0), coeffs=[2, 0])
        }
    }

    simulation = Simulation(boundaries, 
                            agents_A = agents_A, 
                            agents_B = agents_B, 
                            strategy_A = args.strategy_A, 
                            strategy_B = args.strategy_B,
                            suppliers = suppliers, 
                            warehouses = warehouses,
                            potentials = potentials)

    simulation.run(n_step=args.n_step)

    if args.save is not None:
        simulation.save(args.save)


if __name__=='__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-n", "--n_step", type=int, default=300)
    parser.add_argument("-A", "--strategy_A", type=str, required=True)
    parser.add_argument("-B", "--strategy_B", type=str, required=True)
    parser.add_argument("-s", "--save", type=str)

    args = parser.parse_args()

    main(args)
