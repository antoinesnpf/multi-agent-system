import copy
import random

import numpy as np

from src.definition import SPEED_A, SPEED_B
from src.tools import get_unit_vect, normalize, vect_zero


class BaseAgent:
    def __init__(self, gender, initial_pos, boundaries):
        assert gender in ['A', 'B']
        self.gender = gender
        self.is_dead = False
        self.is_loaded = False
        self.pos = copy.copy(initial_pos)
        self.new_pos = None
        self.boundaries = boundaries
        self.speed = SPEED_A if gender == 'A' else SPEED_B
        
    def _check_boundaries(self):
        for i, boundary in enumerate(self.boundaries):
            if self.pos[i] < boundary[0]:
                self.pos[i] = boundary[0]
                self.is_dead = True
            if self.pos[i] > boundary[1]:
                self.pos[i] = boundary[1]
                self.is_dead = True
    
    def _compute_new_pos(self, strategy, agents_A, agents_B, suppliers, warehouses, potentials):
        
        assert strategy in ['random', 'naive', 'potential', 'evolution']
        
        if strategy == 'random':
            self.new_pos = self.random_step()        
        elif strategy == 'naive':
            self.new_pos = self.naive_step(agents_A, agents_B, suppliers, warehouses)       
        elif strategy == 'potential':
            self.new_pos = self.potential_step(agents_A, agents_B, suppliers, warehouses, potentials)
        
    def _step(self):
        assert self.new_pos is not None
        self.pos = self.new_pos
        self.new_pos = None
        self._check_boundaries()
        
    def random_step(self):
        'returns the new position of the agent after a random step'
        sign = 1 if random.random() < 0.5 else -1
        return self.pos + self.speed * sign * normalize(np.random.rand(self.pos.size))
    
    def naive_step(self, agents_A, agents_B, suppliers, warehouses):
        """ Returns the new position of the agent after a naive strategy step. 
        Each A-agent follows the same B-agent for the whole simulation. 
        Each B-agent makes rounds trips between the suppliers and the warehouses """
        if self.gender == 'A':
            if len(agents_B) > 0: 
                target = agents_B[-1]
                obj = target.pos
            else: obj = self.pos
                
            return self.pos + self.speed * normalize(obj - self.pos)
        
        if self.gender == 'B':
            if self.is_loaded:
                target = warehouses[-1]
            else:
                target = suppliers[-1]
            return self.pos + self.speed * normalize(target.pos - self.pos)
        
    def potential_step(self, agents_A, agents_B, suppliers, warehouses, potentials : dict):
        agent_list = agents_A + agents_B # self should be excluded from the list bc we don't want an agent to affect its own movement
        resulting_grad = vect_zero(len(self.pos))

        for agent in agent_list:
            potential = potentials[self.gender][agent.gender]
            vect_grad = potential.vect_grad(agent.pos, self.pos)
            resulting_grad += vect_grad
        if self.gender == 'B':
            if agent.is_loaded:
                for warehouse in warehouses:
                    potential = potentials[self.gender]['warehouses']
                    vect_grad = potential.vect_grad(warehouse.pos, self.pos)
                    resulting_grad += vect_grad
            else:
                for supplier in suppliers:
                    potential = potentials[self.gender]['suppliers']
                    vect_grad = potential.vect_grad(supplier.pos, self.pos)
                    resulting_grad += vect_grad

        env_potential = potentials[self.gender]['environment']
        vect_grad = env_potential.vect_grad(self.pos)
        resulting_grad += vect_grad
        normalized_grad = get_unit_vect(resulting_grad, 0)
        return self.pos + self.speed * normalized_grad
    
    def evolution_step(self):
        pass
        
class Objective:
    def __init__(self, initial_pos):
        self.pos = initial_pos
