import numpy as np
from src.agents import BaseAgent, Objective

BOUNDARIES = np.array([[-5, 5], [-5, 5]]).astype('float64')
COLLISION_THRESHOLD = 0.3  # euclidian distance under which two agents are touching
SPEED_A = 0.05
SPEED_B = 0.1


EFFECTIF_A = 3
EFFECTIF_B = 6

INITIAL_POS_A = [[-4, -4] ,[-3, -4], [-4, -3]]
INITIAL_POS_B = [[-2, 4], [0, 4], [4, 4], [4, 0], [4, -2], [3, 3]]
INITIAL_POS_A = [np.array(u).astype('float64') for u in INITIAL_POS_A]
INITIAL_POS_B = [np.array(u).astype('float64') for u in INITIAL_POS_B]

INITIAL_POS_W = [[-3, 0], [3, 0], [0, 3], [0, -3]]
INITIAL_POS_W = [np.array(u).astype('float64') for u in INITIAL_POS_W]
INITIAL_POS_S = np.array([0, 0])

AGENTS_A = [BaseAgent('A',INITIAL_POS_A[i] ,BOUNDARIES) for i in range(EFFECTIF_A)]
AGENTS_B = [BaseAgent('B',INITIAL_POS_B[i] ,BOUNDARIES) for i in range(EFFECTIF_B)]
WAREHOUSES = [Objective(pos) for pos in INITIAL_POS_W]
SUPPLIERS = [Objective(INITIAL_POS_S)]