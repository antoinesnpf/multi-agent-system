from src.tools import get_unit_vect, derivative_power_series, get_distance, vect_zero


class Potential:
    def __init__(self, power_range, coeffs):
        self.power_range = power_range
        self.coeffs = coeffs
    
    def vect_grad(self, potential_center, agent_pos):
        distance = get_distance(potential_center, agent_pos)
        if distance != 0:
            gradient_norm = derivative_power_series(self.power_range, self.coeffs, distance)
            unit_vect = get_unit_vect(agent_pos, potential_center)
            return unit_vect * gradient_norm
        else:
            return vect_zero(len(agent_pos))

        
class EnvPotential:
    def __init__(self, power_range, coeffs, size):
        self.size = size
        self.power_range = power_range
        self.coeffs = coeffs
    
    def vect_grad(self, agent_pos):
        vect_grad = vect_zero(len(agent_pos))
        
        for i in range(len(agent_pos)):
            d1 = self.size + agent_pos[i]
            d2 = self.size - agent_pos[i]
            vect_grad[i] = derivative_power_series(self.power_range, self.coeffs, d1) - derivative_power_series(self.power_range, self.coeffs, d2)
        
        return - vect_grad

