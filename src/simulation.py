
import pickle
import os

from src.definition import COLLISION_THRESHOLD, SPEED_A, SPEED_B
from src.tools import get_distance


class Simulation:
    def __init__(self, boundaries, agents_A, agents_B, strategy_A, strategy_B, suppliers, warehouses, potentials=None):
        self.boundaries = boundaries
        self.agents_A = agents_A
        self.agents_B = agents_B
        self.strategy_A = strategy_A
        self.strategy_B = strategy_B
        self.suppliers = suppliers
        self.warehouses = warehouses
        self.potentials = potentials
        self.score = 0
    
    def _step(self):
        for agent in self.agents_A:
            agent._compute_new_pos(strategy = self.strategy_A,
                                   agents_A = self.agents_A,
                                   agents_B = self.agents_B,
                                   suppliers = self.suppliers,
                                   warehouses = self.warehouses,
                                   potentials = self.potentials)
            
        for agent in self.agents_B:
            agent._compute_new_pos(strategy = self.strategy_B, 
                                   agents_A = self.agents_A,
                                   agents_B = self.agents_B,
                                   suppliers = self.suppliers,
                                   warehouses = self.warehouses, 
                                   potentials = self.potentials)
            
        for agent in self.agents_A:
            agent._step()
        for agent in self.agents_B:
            agent._step()

    def _get_current_state(self):
        les_xA = [agent_A.pos[0] for agent_A in self.agents_A]
        les_yA = [agent_A.pos[1] for agent_A in self.agents_A]

        les_xB = [agent_B.pos[0] for agent_B in self.agents_B]
        les_yB = [agent_B.pos[1] for agent_B in self.agents_B]
        
        return les_xA, les_yA, les_xB, les_yB
    
    def _check_collisions(self):
        for agent_A in self.agents_A:
            for agent_B in self.agents_B:
                if get_distance(agent_A.pos, agent_B.pos) < COLLISION_THRESHOLD:
                    agent_B.is_dead = True
    
    def _check_suppliers(self):
        for agent_B in self.agents_B:
            for supplier in self.suppliers:
                if get_distance(agent_B.pos, supplier.pos) < COLLISION_THRESHOLD:
                    agent_B.is_loaded = True
        
    def _check_warehouses(self):
        for agent_B in self.agents_B:
            for warehouse in self.warehouses:
                if get_distance(agent_B.pos, warehouse.pos) < COLLISION_THRESHOLD:
                    if agent_B.is_loaded:
                        self.score += 1
                        agent_B.is_loaded = False

    def _remove_dead_agents(self):
        for agents in [self.agents_A, self.agents_B]:
            i = 0
            while i < len(agents):
                if agents[i].is_dead:
                    agents.pop(i)
                else:
                    i += 1
    
    def init_(self):
        les_xA, les_yA, les_xB, les_yB = self._get_current_state()
        
        les_xS = [supplier.pos[0] for supplier in self.suppliers]
        les_yS = [supplier.pos[1] for supplier in self.suppliers]
        les_xW = [warehouse.pos[0] for warehouse in self.warehouses]
        les_yW = [warehouse.pos[1] for warehouse in self.warehouses]
        
        initial_state = (les_xA, les_yA, les_xB, les_yB)
        env_state = (les_xS, les_yS, les_xW, les_yW)
        
        return [initial_state], env_state 
    
    def run(self, n_step):
        self.compteur = 0
    
        self.states, self.env_state = self.init_()
        
        try : 
            for k in range(n_step) : 
                self._step()
                self._check_collisions()
                self._check_suppliers()
                self._check_warehouses()
                self._remove_dead_agents()
                self.states.append(self._get_current_state())
                self.compteur += 1
                
        except KeyboardInterrupt : 
            return 'End of Simulation at tick {}'.format(self.compteur)
        
        return self.score

    def save(self, name):
        L_data = [self.states, self.env_state, self.boundaries]
        pickle.dump(L_data, open(os.path.join(os.getcwd(), 'simulation-data', f'{name}.pickle'), mode='wb'))
        print('data saved')

        
