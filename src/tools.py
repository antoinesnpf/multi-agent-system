import numpy as np

def derivative_power_series(power_range, coeffs, distance):
    S = 0
    for i, order in enumerate(power_range):
        S += order * coeffs[i] * np.exp((order - 1) * np.log(distance))
    return S

def normalize(vector):
    "divides the input vector by its L2 norm"
    norm = np.linalg.norm(vector)
    if norm != 0: return vector/norm
    else: return np.zeros_like(vector)

def get_distance(x1, x2):
    " returns the euclidian distance between x1 and x2"
    return np.linalg.norm(x1 - x2)

def get_unit_vect(x, y):
    return normalize(x - y)

def get_vect_norm(x):
    return np.linalg.norm(x)

def vect_zero(n):
    return np.zeros(n)



